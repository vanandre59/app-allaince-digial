<?php

namespace App\Entity;

use App\Repository\TacheRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TacheRepository::class)]
class Tache
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $Nom = null;

    #[ORM\Column(length: 255)]
    private ?string $Description = null;

    // #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    // private ?\DateTimeInterface $Debut = null;

    // #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    // private ?\DateTimeInterface $Fin = null;

    #[ORM\Column]
    private ?bool $Terninee = null;

    #[ORM\Column]
    private ?bool $allDay = null;

    #[ORM\Column(length: 7)]
    private ?string $backgroundColor = null;

    #[ORM\Column(length: 250, nullable: true)]
    private ?string $Commentaire = null;

    #[ORM\Column(length: 4, nullable: true)]
    private ?string $Duree = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $start = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $end = null;

    #[ORM\ManyToOne(inversedBy: 'taches')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Client $Fk_client = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    // public function getDebut(): ?\DateTimeInterface
    // {
    //     return $this->Debut;
    // }

    // public function setDebut(\DateTimeInterface $Debut): self
    // {
    //     $this->Debut = $Debut;

    //     return $this;
    // }

    // public function getFin(): ?\DateTimeInterface
    // {
    //     return $this->Fin;
    // }

    // public function setFin(\DateTimeInterface $Fin): self
    // {
    //     $this->Fin = $Fin;

    //     return $this;
    // }
    public function isTerninee(): ?bool
    {
        return $this->Terninee;
    }

    public function setTerninee(bool $Terninee): self
    {
        $this->Terninee = $Terninee;

        return $this;
    }

    public function isAllDay(): ?bool
    {
        return $this->allDay;
    }

    public function setAllDay(bool $allDay): self
    {
        $this->allDay = $allDay;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    

    public function getCommentaire(): ?string
    {
        return $this->Commentaire;
    }

    public function setCommentaire(?string $Commentaire): self
    {
        $this->Commentaire = $Commentaire;

        return $this;
    }

    public function getDuree(): ?string
    {
        return $this->Duree;
    }

    public function setDuree(?string $Duree): self
    {
        $this->Duree = $Duree;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getFkClient(): ?Client
    {
        return $this->Fk_client;
    }

    public function setFkClient(?Client $Fk_client): self
    {
        $this->Fk_client = $Fk_client;

        return $this;
    }
}
