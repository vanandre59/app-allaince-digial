<?php

namespace App\Controller;

use App\Repository\TacheRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlannigController extends AbstractController
{
    #[Route('/plannig', name: 'app_plannig')]
    public function index(TacheRepository $tacheRepository): Response
    {
        $events = $tacheRepository->findAll();


        $taches = [];
        foreach($events as $event){
            $taches[] = [
                'id' => $event->getId(),
                'title' => $event->getNom(),
                'Description' => $event->getDescription(),
                'start' => $event->getstart()->format('Y-m-d H:i:s'),
                'end' => $event->getend()->format('Y-m-d H:i:s'),
                'Terninee' => $event->isTerninee(),
                'allDay' => $event->isAllDay(),
                'BackgroundColor' => $event->getBackgroundColor(),
                'Commentaire' => $event->getCommentaire(),
                'Duree' => $event->getDuree(),

            ];
        } $data = json_encode($taches);

        return $this->render('plannig/index.html.twig', compact('data'));
    }
}
